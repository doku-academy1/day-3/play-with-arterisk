import java.util.*;

public class Main {
    public static void main(String[] args) {
        Integer number;

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Masukkan angka: ");
        number = keyboard.nextInt();

        for (int i = 0; i<number; i++) {
             for (int j = number-i; j>1; j--) {
                System.out.print(" ");
             }
             for (int j = 0; j<=i; j++) {
                 System.out.print("* ");
             }
             System.out.println();
        }
    }
}